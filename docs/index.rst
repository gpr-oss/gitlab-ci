.. Sphinx documentation master file.

:gitlab_url: https://gitlab.com/gpr-oss/gitlab-ci

Introduction
============

This project provides a collection of Gitlab-CI templates.

.. toctree::
   :maxdepth: 2
   :caption: Template Documentation

   autotag
   python-tox
   sphinx-doc

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

