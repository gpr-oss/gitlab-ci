<!---
Please read this!

Before opening a new issue, make sure to search for keywords in the issues filtered by the "issue::type::feature" or 
"issue::type::story" label and verify the issue you're about to submit isn't a duplicate.
--->

/label ~"issue::type::feature" ~"priority::2"

<!-- 
Un-comment the line for the applicable uniq topic.
Note that all text on that line is deleted upon issue creation.
-->

<!-- /label ~"topic::ux"              - The issue is related to user experience. -->
<!-- /label ~"topic::system"          - The issue is related to system architecture. -->
<!-- /label ~"topic::security"        - The issue is related to security. -->
<!-- /label ~"topic::performance"     - The issue is related to performance. -->
<!-- /label ~"topic::robustness"      - The issue is related to robustness. -->
<!-- /label ~"topic::maintainability" - The issue is related to quality. -->


## Problem to solve

<!-- 
What problem do we solve? 
-->

## Further details

<!-- 
Include use cases, benefits, and/or goals 
-->

## Proposal

<!-- 
How are we going to solve the problem? 
-->

## What does success look like, and how can we measure that?

<!-- 
If no way to measure success, link to an issue that will implement a way to measure this 
-->

## Links / references
