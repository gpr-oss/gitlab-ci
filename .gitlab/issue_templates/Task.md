
/label ~"issue::type::task" ~"priority::2"

<!--
Un-comment the line for the applicable uniq topic.
Note that all text on that line is deleted upon issue creation.
-->

<!-- /label ~"topic::ux"              - The issue is related to user experience. -->
<!-- /label ~"topic::internal"        - The issue is related to internal behavior. -->
<!-- /label ~"topic::security"        - The issue is related to security. -->
<!-- /label ~"topic::performance"     - The issue is related to performance. -->
<!-- /label ~"topic::robustness"      - The issue is related to robustness. -->
<!-- /label ~"topic::maintainability" - The issue is related to quality. -->

<!--
Un-comment the line for the applicable uniq topic.
Note that all text on that line is deleted upon issue creation.
-->

<!-- /label ~"task::develop"     - Development task. -->
<!-- /label ~"task::integration" - Integration task. -->
<!-- /label ~"task::test"        - Testing task. -->
<!-- /label ~"task::delivery"    - Delivery task. -->
<!-- /label ~"task::release"     - Release task. -->
<!-- /label ~"task::doc"         - Documentation task. -->

## Task Summary

<!--
Summarize the task
-->
