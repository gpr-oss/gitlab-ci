<!---
Please read this!

Before opening a new issue, make sure to search for keywords in the issues
filtered by the "issue::type::cve" label and verify the issue you're about
to submit isn't a duplicate.
--->

/label ~"issue::type::cve" ~"priority::2" ~"severity::2"

## CVE Summary

<!--
Summarize the CVE and fill the table.
-->

| ID            | Score | Link                                           |
| ------------- | ----- |----------------------------------------------- |
| CVE-XXXX-XXXX | D.D   | https://nvd.nist.gov/vuln/detail/CVE-XXXX-XXXX |

## Possible fixes

<!-- 
List existing patches or suggest a fix here.
-->