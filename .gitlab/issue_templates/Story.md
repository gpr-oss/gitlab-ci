<!---
Please read this!

Before opening a new issue, make sure to search for keywords in the issues filtered by the "issue::type::story" label and 
verify the issue you're about to submit isn't a duplicate.
--->

/label ~"issue::type::story" ~"priority::2"

## Story Summary

<!-- Describe the story -->

**As a** _(type of user)_
**I want** _(some goal)_
**So that** _(some reason)_

## Acceptance test

<!-- Describe the acceptance test -->

**(Given)** _(some context)_
**(When)** _(some action is carried out)_
**(Then)** _(a particular set of observable consequences should obtain)_
