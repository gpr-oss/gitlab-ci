<!---
Please read this!

Before opening a new issue, make sure to search for keywords in the issues
filtered by the "issue::type::bug" label and verify the issue you're about
to submit isn't a duplicate.
--->

/label ~"issue::type::bug" ~"priority::2" ~"severity::2"

<!-- 
Un-comment the lines for the applicable bug labels.
Note that all text on that line is deleted upon issue creation.
-->

<!-- /label ~"regression" - The bug is a regression. -->

<!-- 
Un-comment the line for the applicable uniq topic.
Note that all text on that line is deleted upon issue creation.
-->

<!-- /label ~"topic::ux"              - The issue is related to user experience. -->
<!-- /label ~"topic::system"          - The issue is related to system architecture. -->
<!-- /label ~"topic::security"        - The issue is related to security. -->
<!-- /label ~"topic::performance"     - The issue is related to performance. -->
<!-- /label ~"topic::robustness"      - The issue is related to robustness. -->
<!-- /label ~"topic::maintainability" - The issue is related to quality. -->

## Bug Summary

<!--
Summarize the bug encountered concisely
-->

## Steps to reproduce

<!--
How one can reproduce the issue - this is very important
-->

## What is the current *bug* behavior?

<!--
What actually happens
-->

## What is the expected *correct* behavior?

<!--
What you should see instead 
-->

## Relevant logs and/or screenshots

<!-- 
Paste any relevant logs - please use code blocks (```) to format console output,
logs, and code as it's very hard to read otherwise. 
-->

## Possible fixes

<!-- 
If you can, link to the line of code that might be responsible for the problem
-->
