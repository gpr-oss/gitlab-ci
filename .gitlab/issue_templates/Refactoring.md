<!---
Please read this!

Before opening a new issue, make sure to search for keywords in the issues
filtered by the "issue::type::refactoring" label and verify the issue you're about
to submit isn't a duplicate.
--->

/label ~"issue::type::refactoring" ~"priority::4"

<!-- 
Un-comment the line for the applicable uniq topic.
Note that all text on that line is deleted upon issue creation.
-->

<!-- /label ~"topic::security"        - The issue is related to security. -->
<!-- /label ~"topic::performance"     - The issue is related to performance. -->
<!-- /label ~"topic::robustness"      - The issue is related to robustness. -->
<!-- /label ~"topic::maintainability" - The issue is related to quality. -->

## Refactoring Summary

<!--
Please briefly describe what part of the code base needs to be refactored.
-->

## Improvements

<!--
Explain the benefits of refactoring this code.
See also https://about.gitlab.com/handbook/values/index.html#say-why-not-just-what
-->

## Risks

<!--
Please list features that can break because of this refactoring and how you intend to solve that.
-->

## Involved components

<!--
List files or directories that will be changed by the refactoring.
-->

## Optional: Intended side effects

<!--
If the refactoring involves changes apart from the main improvements (such as a better UI), list them here.
It may be a good idea to create separate issues and link them here.
-->


## Optional: Missing test coverage

<!--
If you are aware of tests that need to be written or adjusted apart from unit tests for the changed components,
please list them here.
-->
