<!-- 
Use this description template for preparing a new release (tag on stable branch)
-->

/label ~"mr::type::release" ~"priority::0"

## What does this MR do?

This MR prepares the next release.

## Author's checklist

<!-- 
Delete task if not relevant
-->

- [ ] Complete the [Changelog](ChangeLog.md)
- [ ] Bump the version number
- [ ] Complete the documentation

## Review checklist

<!--
Delete task if not relevant
-->

- [ ] Version number Ok
- [ ] Changelog Ok
- [ ] Documentation Ok
