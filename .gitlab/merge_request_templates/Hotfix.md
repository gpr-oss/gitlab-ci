<!-- 
Use this description template for integrating a hotfix to the stable branch
-->

/label ~"mr::type::hotfix" ~"priority::0"

## What does this MR do?

This MR integrates an Hotfix to the stable branch.

## Author's checklist

<!-- 
Delete the line if not relevant
-->

- [ ] Describe tests below

## Tests

<!-- 
Describe the tests and their environments
-->

## Review checklist

<!-- 
Delete the line if not relevant
-->

- [ ] Review Ok
- [ ] Test Ok

## Related issues

<!-- 
Mention the issue(s) this MR closes 
-->

<!-- Closes <ISSUE-ID>  - ref to issues -->
<!-- Relates to <ISSUE-ID> - ref to issues -->
