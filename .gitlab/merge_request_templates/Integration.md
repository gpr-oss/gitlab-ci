<!-- 
Use this description template for integrating development branch to the stable branch.
-->

/label ~"mr::type::integration" ~"priority::2"

## What does this MR do?

This MR integrates changes to the stable branch.

<!-- 
Describe here the changes that are not listed in the following section

Add the following section in case of breaking change.

### Breaking Changes

/label ~"breaking-change"
/award :fire:
 
-->

## Sanity Tests

<!-- 
Describe the sanity tests performed. 
-->

## Related issues

<!-- 
Mention the issue(s) this MR closes 
-->

<!-- Closes <ISSUE-ID>  - ref to issues -->
<!-- Relates to <ISSUE-ID> - ref to issues -->
