# Makefile

PIPENV_VENV_IN_PROJECT = yes
export PIPENV_VENV_IN_PROJECT

.venv/bin/python:
	pipenv install --dev

.PHONY: docs
docs: .venv/bin/python
	pipenv run make -C docs html

.PHONY: bump-%
bump-%:
	pipenv run bumpversion --verbose $*