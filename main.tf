# main.tf -- Terraform test resource

resource "local_file" "test" {
    content     = "test!"
    filename = "${path.module}/tmp/test.txt"
}
