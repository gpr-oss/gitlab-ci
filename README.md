# gitlab-ci

Collection of Gitlab CI/CD configuration to be included in other project.

## `autotag.gitlab-ci.yml`

Use bumpversion and git-chglog to automatically:
- bump the version number
- generate the Changelog
- create tag
- create the Gitlab release for the new tag

## `sphinx-doc.gitlab-ci.yml`

Generate sphinx doc to be published as Gitlab pages.
